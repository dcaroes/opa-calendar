#!/usr/bin/env sh
/*usr/bin/env true
export NODE_PATH="$NODE_PATH:.:/usr/lib/opa/static:/usr/lib/opa/stdlib:/usr/lib/opa/stdlib/stdlib.qmljs:`which npm > /dev/null 2>&1 && npm root -g`:node_modules:"


node=`which node 2>&1`
if [ $? -ne 0 ] || [ ! -x "$node" ]; then

    NODE_VERSION=v0.8.7

    # Detect OS
    IS_LINUX=""
    IS_MAC=""
    IS_WINDOWS=""
    IS_FREEBSD=""
    case $(uname) in
    CYGWIN*) IS_WINDOWS=1;;
    Darwin*) IS_MAC=1;;
    Linux*|GNU/kFreeBSD) IS_LINUX=1;;
    FreeBSD) IS_FREEBSD=1;;
    *)
            echo "Error: could not detect OS. Defaulting to Linux" >&2
            IS_LINUX=1
    esac

    echo "node.js is missing, Download and install it ? (no will abort) [Yn] \c"
    read yesno
    case "$yesno" in
        y|Y|yes|YES)
        if [ -n "$IS_MAC" ]; then
        port=`which port 2>&1`
        if [ $? -eq 0 ] && [ -x "$port" ]; then
            echo "--> Installing via MacPorts...";
            sudo port install nodejs
        else
            brew=`which brew 2>&1`
            if [ $? -eq 0 ] && [ -x "$brew" ]; then
            echo "--> Installing via Homebrew...";
            brew install node # Warning: brew installs are known to be buggy
            else
            if ! [ -f /tmp/node-$NODE_VERSION.pkg ]; then
                NODE_URL=http://nodejs.org/dist/$NODE_VERSION/node-$NODE_VERSION.pkg
                echo "--> Downloading $NODE_URL..."
                curl $NODE_URL > /tmp/node-$NODE_VERSION.pkg
            fi
            echo "--> Opening Node installer $NODE_VERSION, please follow the instructions and then relaunch this application"
            open /tmp/node-$NODE_VERSION.pkg
        exit 1
            fi
        fi
        elif [ -n "$IS_LINUX" ]; then
        case $(uname -v) in
            *Ubuntu*)
            sudo apt-get install python-software-properties
            sudo add-apt-repository ppa:chris-lea/node.js
            sudo apt-get update
            sudo apt-get install nodejs npm
            ;;
            *)
            echo "--> node.js is missing, please install node.js from: http://nodejs.org"
            exit 1
        esac
        else
        echo "--> node.js is missing, please install node.js from: http://nodejs.org"
        exit 1

        fi;;
    *) echo "--> Aborting..."; exit 1
    esac

fi;

if [ $? -ne 0 ]; then exit $?; fi;
node "$0" "$@"; exit $?;

*/


var min_node_version = 'v0.6.0';

if (process.version < min_node_version) {
    console.error('Your version of node seems to be too old. Please upgrade to a more recent version of node (>= '+min_node_version+')');
    process.exit(1);
}


require('opa-js-runtime-cps');
check_opa_deps(['opabsl.opp', 'formidable']);
require("opa-js-runtime-cps");
require("opabsl.opp");
require("html5.opx");
var ___server_ast = BslCps_topwait(___register_server_code_ec6de8cf({package_:"",adhoc_e:[{td:[],rd:[],r:[true],id:["`_v1_Server_private_run_services_stdlib.core.web.server`"],i:{some:"_v1_run_services"},d:js_void,c:{none:js_void}}]},cont(BslCps_topk)));
var ___run_services = BslCps_topwait(___run_services_b970f080(cont(BslCps_topk)));
