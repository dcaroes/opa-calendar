

/**
* @register {string -> int}
*/
function getStorageInt(itemName){
	return localStorage.getItem(itemName);
}

/**
* @register {string -> string}
*/
function getStorageString(itemName)
{
	return localStorage.getItem(itemName);
}

/**
* @register {string, int -> void}
*/
function setStorageInt(itemName, value){
	localStorage.setItem(itemName, value);
}

/**
* @register {string, string -> void}
*/
function setStorageString(itemName, value){
	localStorage.setItem(itemName, value);
}

/**
* @register {-> void}
*/
function clearStorage(){
	localStorage.clear();
}

/**
* @register {-> bool}
*/
function checkOnline(){
	var x=navigator.onLine;
	alert(""+x):
	if (x) {
		true;	
	}else{
		false;	
	}
}


